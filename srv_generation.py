import json
import os
import psutil
from random import randint
from bottle import Bottle, run, request
from rungen.run_generation import rungen

app = Bottle()


@app.get('/info')
def info():
    process = psutil.Process(os.getpid())
    return json.dumps({
        'process_memory_info': {
            'rss': process.memory_info().rss
        },
        'cpu_percent': psutil.cpu_percent(),
        'virtual_memory': {
            'available': psutil.virtual_memory().available,
            'total': psutil.virtual_memory().total
        }
    })


@app.post('/gen')
def gen():
    model_type = request.forms.get('model_type', 'gpt2')
    model_name_or_path = request.forms.get('model_name_or_path', 'gpt2')
    prompt = request.forms.get('prompt')
    length = request.forms.get('length', 20)
    stop_token = request.forms.get('stop_token', '.')
    temperature = request.forms.get('temperature', 1)
    # repetition_penalty = request.forms.get('repetition_penalty')
    # k = request.forms.get('k')
    p = request.forms.get('p', 0.9)
    # padding_text = request.forms.get('padding_text')
    # xlm_language = request.forms.get('xlm_language')
    seed = request.forms.get('seed', randint(0, 1000000))
    # no_cuda = request.forms.get('no_cuda')
    num_return_sequences = request.forms.get('num_return_sequences', 1)

    if model_type == 'dummy':
        return []

    return json.dumps(rungen([
            "--model_type", model_type,
            "--model_name_or_path", model_name_or_path,
            "--prompt", prompt,
            "--length", str(length),
            "--stop_token", stop_token,
            "--temperature", str(temperature),
            "--p", str(p),
            "--seed", str(seed),
            "--num_return_sequences", str(num_return_sequences)
        ]))


print('READY!')
run(app, host='0.0.0.0', port=8080)
