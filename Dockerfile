FROM python:3

WORKDIR /var/www

RUN apt-get update
RUN pip install torch==1.6.0+cpu torchvision==0.7.0+cpu -f https://download.pytorch.org/whl/torch_stable.html
COPY requirements.txt .
RUN pip install -r requirements.txt

RUN mkdir rungen
COPY rungen rungen
COPY srv_generation.py .

EXPOSE 8080
ENTRYPOINT python3 srv_generation.py